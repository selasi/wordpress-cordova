/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
var app = {
    // Application Constructor
    initialize: function() {
        document.addEventListener('deviceready', this.onDeviceReady.bind(this), false);
    },

    // deviceready Event Handler
    //
    // Bind any cordova events here. Common events are:
    // 'pause', 'resume', etc.
    onDeviceReady: function() {
        this.receivedEvent('deviceready');
    },

    // Update DOM on a Received Event
    receivedEvent: function(id) {
        var parentElement = document.getElementById(id);
        var listeningElement = parentElement.querySelector('.listening');
        var receivedElement = parentElement.querySelector('.received');

        listeningElement.setAttribute('style', 'display:none;');
        receivedElement.setAttribute('style', 'display:block;');

        console.log('Received Event: ' + id);
    },
    // Log users out
    logout: function() {
        // Logout codes goes here
        window.location.href = "login.html";
    },
    // Log users in
    login: function() {
        var username = $("#username").val();
        var password = $("#password").val();
        var dataString = "username="+username+"&password="+password+"&insecure=cool";
        $.ajax({
          type: "POST",
          url:"http://amdsela.com/wordpress/api/user/generate_auth_cookie/?",
          data: dataString,
          crossDomain: true,
          cache: false,
          success: function(data){
           //alert(JSON.stringify(data));
           window.location.href = "index.html";
          }
        });
    },
    // Register New Users
    register: function() {
        var display_name = $("#display_name").val();
        var email = $("#email").val();
        var username = $("#username").val();
        var password = $("#password").val();
        $.ajax({
          type: "POST",
          url:"http://amdsela.com/wordpress/api/get_nonce/?controller=user&method=register",
          crossDomain: true,
          cache: false,
          success: function(data){
            var nonce = data.nonce;
            var dataString = "display_name="+display_name+"&email="+email+"&username="+username+"&password="+password+"&nonce="+nonce+"&insecure=cool";
            $.ajax({
              type: "POST",
              url:"http://amdsela.com/wordpress/api/user/register",
              data: dataString,
              crossDomain: true,
              cache: false,
              success: function(data){
                //alert("Registration Successful /n" + JSON.stringify(data));
                window.location.href = "index.html";
              }
            });
           }
         });
    },
    // Get all posts from wordpress
    getPosts: function() {
        var url="http://amdsela.com/wordpress/?json=get_recent_posts";
        $.getJSON(url,function(result){
         $.each(result.posts, function(i, field){
            var title = field.title;
            var content = field.content;
            $("#listview").append("<li>" + title + "<small>" + content + "</small></li>");
         });
        });
    }
};

app.initialize();